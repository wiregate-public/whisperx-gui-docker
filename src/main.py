import os
import hashlib
import shutil
import gradio as gr
import time
from transcribe import transcribe
from whisperx.utils import LANGUAGES

def calculate_md5(file_name, block_size=4096):
    hasher = hashlib.md5()
    with open(file_name, 'rb') as file:
        for chunk in iter(lambda: file.read(block_size), b''):
            hasher.update(chunk)
    return hasher.hexdigest()

def move_file(source_path, destination_path):
    try:
        shutil.move(source_path, destination_path)
        print(f"File moved successfully to {destination_path}")
    except Exception as e:
        # Handle the case where the move operation fails
        print(f"Error occurred while moving the file: {e}")

def map_language(language):
    return None if language == "auto" else find_language_code(language)

def find_language_code(language):
    for key, value in LANGUAGES.items():
        if language == value:
            return key

def create_dir_if_not_exist(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)

def write_content(file_name, content):
    f = open(file_name, "w")
    f.write(content)
    f.close()

def process_file(model, language, temp_file, progress=gr.Progress()):
    start_time = time.time()
    if not temp_file or not os.path.exists(temp_file):
        return "Please upload a file before submitting."
    language = map_language(language)
    create_dir_if_not_exist("upload")
    file_path, file_extension = os.path.splitext(temp_file)
    md5_hash = calculate_md5(temp_file)
    output_file = os.path.join("upload", f"{md5_hash}{file_extension}")
    move_file(temp_file, output_file)
    srt_content = transcribe(model, language, output_file, progress)
    output_file_srt = f"{file_path}.srt"
    write_content(output_file_srt, srt_content)
    execution_time = time.time() - start_time
    data = (
        f"{file_path}.srt",
        srt_content,
        f"Execution Time: {execution_time:.0f}s"
    )
    return data

def main():
    inputs = [
        gr.Dropdown(
            ["medium", "large-v3"],
            value="medium",
            multiselect=False,
            label="Whisper Model"
        ),
        gr.Dropdown(
            ["auto"] + list(LANGUAGES.values()),
            value="auto",
            multiselect=False,
            label="Language"
        ),
        gr.File(
            type="filepath",
            file_types=["audio", "video"],
            label="Upload your video or audio file"
        )
    ]
    outputs = [
        gr.DownloadButton(label="Download Processed File"),
        gr.Textbox(label="Processed Text"),
        gr.Textbox(label="Execution Time")
    ]
    iface = gr.Interface(
        fn=process_file,
        inputs=inputs,
        outputs=outputs,
        title="WhisperX debug interface",
        description="Upload your audio or video file.",
        allow_flagging="never"
    )

    iface.launch()

if __name__ == "__main__":
    main()

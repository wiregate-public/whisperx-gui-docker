FROM nvidia/cuda:12.1.1-base-ubuntu22.04

ENV PIP_NO_WARN_ABOUT_ROOT_USER=0

WORKDIR /app

RUN apt update; apt install -y git ffmpeg python3-pip
COPY requirements.txt /app/
RUN pip install -r requirements.txt \
    && ffmpeg -f lavfi -i anullsrc=channel_layout=mono:sample_rate=44100 -t 1 -c:a aac dummy.aac \
    && for MODEL in "medium" "large-v3"; do whisperx --model ${MODEL} --compute_type int8 dummy.aac; done \
    && rm -f dummy.*

COPY src /app/src

ENV GRADIO_SERVER_NAME=0.0.0.0
ENV OPENBLAS_NUM_THREADS=1

ENTRYPOINT ["python3", "/app/src/main.py"]

--extra-index-url https://download.pytorch.org/whl/cu121
ctranslate2==4.4.0
gradio==5.5.0
numpy==1.26.4
torch==2.2.0
torchvision==0.17.0
torchaudio==2.2.0
whisperx==3.3.1

# Docker for whisperx-gui
Docker image that contains all required dependencies for the
[whisperx](https://github.com/m-bain/whisperX) and

## Launch 🚀
Start docker container:
```
docker compose up
```
Open in the browser: http://localhost:7860

### docker compose profiles
Starting with optionals profiles:
```
docker compose --profile XXXXX up
```
- `gitlab-gpu` - start image downloaded from GitLab with Nvidia GPU support;
- `gitlab-cpu` (default) - start image downloaded from Gitlab in CPU-only mode;
- `local-gpu` - build Dockerfile and start with Nvidia GPU support;
- `local-cpu` - build Dockerfile and start in CPU-only mode;
